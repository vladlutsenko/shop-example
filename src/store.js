import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        products: []
    },
    getters: {
        getProducts: (state) => {
            return state.products
        }
    },
    mutations: {
        addProduct: (state, p)=> {
            let el;
            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id==p.id) {
                    el=i;
                    break;
                }
            }
            if (el>=0) {
                state.products[el].quantity++;
                //state.products[el].total_price=state.products[el].price*state.products[el].quantity;
            }
            else{
                state.products.push({
                    ...p,
                    quantity: 1
                });
                state.products[state.products.length-1].quantity=1;
                //state.products[state.products.length-1].total_price=state.products[state.products.length-1].price*1;
            }
            
        },
        deleteProduct: (state, id)=>{
            let el;
            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id==id) {
                    el=i;
                    break;
                }
            }
            state.products.splice(el,1);
        },
        plus: (state, id)=>{
            let el;
            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id==id) {
                    el=i;
                    break;
                }
            }
            state.products[el].quantity++;
            //state.products[el].total_price=state.products[el].price*state.products[el].quantity;
        },
        minus: (state, id)=>{
            let el;
            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id==id) {
                    el=i;
                    break;
                }
            }
            state.products[el].quantity--;
            //state.products[el].total_price=state.products[el].price*state.products[el].quantity;
        },
        
        clear: (state)=> state.products=[],
    },
    actions: {},
});
export default store